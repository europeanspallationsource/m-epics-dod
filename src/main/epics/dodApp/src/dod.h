/*!
 * \file dod.h
 * \brief Data-on-Demand implementation
 * \author Niklas Claesson <niklas.claesson@cosylab.com>
 */
#ifndef DOD_H

#define DOD_H

#ifndef DEBUG
#   define DEBUG 0
#endif

#ifndef TRACE
#   define TRACE 0
#endif

/*!
 * This struct is used as buffer element
 */
struct buf_item {
        unsigned int   nelm;      /*!< Number of elements in raw data */
        int            requested; /*!< Flag to indiciate if it is requested */
        epicsTimeStamp time;      /*!< Timestamp for raw data */
        void          *data;      /*!< Raw data */
};

/*!
 * This struct contains internal data to the buffer process
 */
struct buf_priv {
        struct buf_item        *buf;       /*!< Buffer first element */
        struct buf_item        *head;      /*!< Buffer head */
        struct buf_item        *tail;      /*!< Buffer tail */
        unsigned int            toArchive; /*!< Number of elements left to archive
                                                counter */
        int                     watchdog;  /*!< The watchdog increases for every read
                                                and gets reset by writes. */
        int                     initFill;  /*!< 1 If filled once */
        unsigned int            bSize;     /*!< Buffer size (number of arrays in
                                                buffer) */
        unsigned int            cBunch;    /*!< Bunch array length */
        void                   *bunchVal;  /*!< Bunch values */
        struct epicsTimeStamp  *bunchTs;   /*!< Bunch Timestamps */
        /* Debug variables */
        unsigned int     writes;
        unsigned int     reads;
};

#endif /* DOD_H */
