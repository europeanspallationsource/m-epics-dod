/*!
 * Data on Demand
 * Copyright (C) 2014 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \file dod.c
 * \brief Data-on-Demand implementation
 *
 * Enable DEBUG or TRACE support with -DDEBUG=1, -DDEBUG=2 and -DTRACE=1
 * \author Niklas Claesson <niklas.claesson@cosylab.com>
 */
#include <stdio.h>
#include <string.h>

#include <dbDefs.h>
#include <dbAccess.h>
#include <dbEvent.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <epicsTime.h>
#include <cantProceed.h>
#include <errlog.h>
#include <recGbl.h>
#include <alarm.h>

#include "dod.h"

#define debug_print(fmt, ...) \
        do { if (DEBUG > 0 && *(epicsInt32 *) precord->g) fprintf(stderr, "[D] %25s %15s(): " fmt, precord->name, __func__, __VA_ARGS__); } while (0)
#define debug2_print(fmt, ...) \
        do { if (DEBUG > 1 && *(epicsInt32 *) precord->g) fprintf(stderr, "[D] %25s %15s(): " fmt, precord->name, __func__, __VA_ARGS__); } while (0)
#define trace_print() \
        do { if (TRACE > 0 && *(epicsInt32 *) precord->g) fprintf(stderr, "[T] %25s %15s()\n", precord->name, __func__); } while (0)

#define COUNT(head, tail, size) \
        (head >= tail ? head - tail : size - (tail - head))

#define MIN(x,y)  (((x) < (y)) ? (x) : (y))

#define BUFFER_SLACK 4

/*!
 * This function is run when the record is initialized. It allocates the memory
 * required for private data. It also sanity checks the values for array lengths.
 * The DPVT field in the record is used as private storage.
 *
 * \param[in,out] precord Processed record.
 */
static long bufferInit(aSubRecord *precord)
{
        int              status = 0;
        struct buf_priv *priv;

        priv = (struct buf_priv *) callocMustSucceed(1, sizeof(struct buf_priv), "Cannot allocate"
                        "space required for DoD application");

        if(precord->noa <= 0 || precord->nova <= 0) {
                errlogPrintf("Array A or array VALB to small. They have to be at least 1.\n");
                status = -1;
        }

        if(precord->noa > precord->nova) {
                errlogPrintf("NOA is bigger than NOVA. All values won't be archived.\n");
                status = -1;
        }

        /* If NOA == 1 the bunching feature is enabled. */
        if(precord->noa == 1) {
                if(precord->novb != precord->novc) {
                        errlogPrintf("Bunch val length (NOVB) and Bunch"
                                        "timestamp length (NOVC) must match.\n");
                        status = -1;
                }

                priv->bunchVal =
                        callocMustSucceed(precord->novb, dbValueSize(precord->fta),
                                        "Cannot allocate space required for DoD"
                                        "application");
                priv->bunchTs  =
                        (epicsTimeStamp *)callocMustSucceed(precord->novc, sizeof(epicsTimeStamp),
                                        "Cannot allocate space required for DoD"
                                        "application");
        }

        if(status) {
                recGblSetSevr(precord,epicsAlarmSoft,epicsSevInvalid);
        }

        /* No data in the bunches yet */
        precord->nevb = 0;
        precord->nevc = 0;
        precord->nevd = 0;
        precord->neve = 0;

        /* Set outputs. */
        precord->dpvt = priv;

        return 0;
}

/*!
 * Method for (re)allocating all buffers. If the buffer is shrinked, only the
 * oldest data will be lost.
 *
 * \param[in,out] precord    Processed record.
 * \param[in]     bufferSize New Number of arrays in buffer.
 */
void buffer_reshape(
                aSubRecord      *precord,
                unsigned int     bufferSize)
{
        struct buf_item *old, *head, *tail, *buf, *itr, *srcFirst;

        unsigned int oldBufferSize;
        epicsUInt32  bMLengthOut;   /* Maximum length of array OUT. */
        size_t       elementSize;   /* Size of each value in array */
        int          status = 0;

        struct buf_priv *priv = (struct buf_priv *) precord->dpvt;

        old           = priv->buf;
        head          = priv->head;
        tail          = priv->tail;
        oldBufferSize = priv->bSize;
        bMLengthOut   = precord->nova;
        elementSize   = dbValueSize(precord->fta);

        priv->bSize = bufferSize; /* We will try and allocate bufferSize. */

        unsigned long totalMem = bufferSize * (bMLengthOut * elementSize + sizeof(struct buf_item));
        debug_print("Allocating %lu MB.\n", totalMem/1024/1024);

        if(bufferSize < oldBufferSize){
                /* Allocate new circular buffer. */
                buf = (struct buf_item *) calloc(bufferSize, sizeof(struct buf_item));
                if(buf == NULL) {
                        errlogPrintf("Cant allocate temporary storage.\n");
                        status = -1;
                }

                /* Calculate from where to start the copying of the buffer. As
                 * many values as possible from head and backwards are stored.  */
                srcFirst = head - bufferSize + 1;

                /* Two cases, either the srcFirst is behind head or ahead of
                 * head. In both cases, the buffer content is memcpyd and the
                 * rest is freed. */
                if(srcFirst >= old) {
                        for(itr = old; itr < srcFirst; ++itr) {
                                free(itr->data);
                        }
                        memcpy(buf, srcFirst, bufferSize * sizeof(struct buf_item));
                        for(itr = head + 1; itr < old + oldBufferSize; ++itr) {
                                free(itr->data);
                        }
                } else  {
                        srcFirst += oldBufferSize;
                        unsigned int tail_len = oldBufferSize - (srcFirst - old);
                        memcpy(buf,            srcFirst, tail_len         * sizeof(struct buf_item));
                        memcpy(buf + tail_len, old,      (head - old + 1) * sizeof(struct buf_item));
                        for(itr = head + 1; itr <= head + (oldBufferSize-bufferSize); ++itr) {
                                free(itr->data);
                        }
                }

                free(old);

                /* Determine content size to set new tail. */
                unsigned int oldCount = COUNT(head, tail, oldBufferSize);

                if(oldCount < bufferSize) {
                        tail = buf + bufferSize - oldCount - 1;
                } else {
                        tail = buf + 1;
                }

                head = buf + bufferSize - 1;
        } else {
                /* (Re)allocate memory for buffer slots. */
                buf = (struct buf_item *)
                        realloc(old, bufferSize * sizeof(struct buf_item));
                if(buf == NULL) {
                        errlogPrintf("Failed to allocate buffer element, "
                                        "you're out of memory...\n");
                        status = -1;
                }

                /* Update head and tail pointers to new buffer */
                tail = buf + (tail-old);
                head = buf + (head-old);

                /* Allocate memory to data field in all new buffer elements */
                for(itr = buf + oldBufferSize; itr < buf + bufferSize; ++itr) {
                        itr->data = calloc(bMLengthOut, elementSize);
                        if(itr->data == NULL) {
                                errlogPrintf("Failed to allocate buffer array "
                                                "bufsize will be %ld, you're "
                                                "out of memory...\n", itr-buf-1);
                                priv->bSize = itr-buf-1;
                                status = -1;
                                break;
                        }
                }

                /* Repartition if required. Swap the old upper region onto the
                 * new upper region. */
                if(tail > head) {
                        /* Memory required to swap */
                        unsigned int swap_len = priv->bSize - oldBufferSize;
                        /* Length from tail pointer to end of old buffer */
                        unsigned int tail_len = oldBufferSize - (tail - buf);
                        struct buf_item *swap =
                                (struct buf_item *) malloc(swap_len * sizeof(struct buf_item));
                        if(swap == NULL){
                                errlogPrintf("Unrecoverable error, couldn't allocate swap area.\n");
                                status = -1;
                        }

                        itr = buf + priv->bSize - swap_len;
                        memcpy(swap,             itr,  swap_len * sizeof(struct buf_item));
                        memmove(tail + swap_len, tail, tail_len * sizeof(struct buf_item));
                        memcpy(tail,             swap, swap_len * sizeof(struct buf_item));
                        free(swap);

                        tail += swap_len;
                }

        }
        priv->head = head;
        priv->tail = tail;
        priv->buf  = buf;
        debug_print("Allocated: %u slots, %lu B, b=%p, h=%p, t=%p\n",
                        bufferSize, totalMem, (void *)buf, (void *)head,
                        (void *)tail);
        if(status) {
                recGblSetSevr(precord,epicsAlarmSoft,epicsSevInvalid);
        }
}

/* Converts epicsTimeStamp to unix milliseconds since epoch. */
epicsFloat64 epicsTimeToMsec(struct epicsTimeStamp *time)
{
        struct timespec unix_time = {0,0};
        epicsTimeToTimespec(&unix_time, time);
        long int msec = unix_time.tv_sec*1e3 + (int)(unix_time.tv_nsec*1.0e-6);
        return msec + (unix_time.tv_nsec%1000000)*(epicsFloat64)1.0e-6;
}

/*!
 * Bunches are first stored privately in the record. This method moves all the
 * internal raw data to the record "interface" (VALB,VALC) and triggers the
 * output links.
 *
 * \param[in,out] precord Processed record
 */
void flip_bunches(aSubRecord *precord)
{
        struct buf_priv *priv;        /* Application specific data */
        void            *bunchVal;    /* Target for archiving data in bunches */
        epicsUInt32     *bunchTsS;    /* Target for archiving timestamps in bunches */
        epicsUInt32     *bunchTsNS;   /* Target for archiving timestamps in bunches */
        epicsFloat64    *bunchTsMS;   /* Target for archiving timestamps in bunches */
        size_t           valueSize;   /* Size of each value in array */
        epicsUInt32     *bunchValNe;  /* Nbr of Values in bunch. */
        epicsUInt32     *bunchTsSNe;  /* Nbr of timestamps in bunch. */
        epicsUInt32     *bunchTsNSNe; /* Nbr of timestamps in bunch. */
        epicsUInt32     *bunchTsMSNe; /* Nbr of timestamps in bunch. */

        priv        = (struct buf_priv *) precord->dpvt;
        bunchVal    =            (void *) precord->valb;
        bunchValNe  =                    &precord->nevb;
        bunchTsS    =     (epicsUInt32 *) precord->valc;
        bunchTsSNe  =                    &precord->nevc;
        bunchTsNS   =     (epicsUInt32 *) precord->vald;
        bunchTsNSNe =                    &precord->nevd;
        bunchTsMS   =    (epicsFloat64 *) precord->vale;
        bunchTsMSNe =                    &precord->neve;
        valueSize   =                     dbValueSize(precord->fta);

        memcpy(bunchVal, priv->bunchVal, (priv->cBunch) * valueSize);
        for(int i = 0; i < priv->cBunch; ++i) {
                bunchTsS[i] = priv->bunchTs[i].secPastEpoch;
                bunchTsNS[i] = priv->bunchTs[i].nsec;
                bunchTsMS[i] = epicsTimeToMsec(&priv->bunchTs[i]);
        }
        *bunchValNe = priv->cBunch;
        *bunchTsSNe  = priv->cBunch;
        *bunchTsNSNe  = priv->cBunch;
        *bunchTsMSNe  = priv->cBunch;
        priv->cBunch = 0;

        debug_print("%s\n", "flipped bunches");

        /* Ugly hack. Triggers b,c,d,e output links and posts monitors even if
         * record wouldn't post...  This needs to be done because these links
         * should not be followed at the same time as the outa link.
         * Copy-pasted from aSubRecord.c */
        for (int i = 0; i < 4; i++)
            dbPutLink(&(&precord->outb)[i], (&precord->ftvb)[i], (&precord->valb)[i],
                (&precord->nevb)[i]);

        unsigned short monitor_mask = recGblResetAlarms(precord) | DBE_VALUE | DBE_LOG;
        for (int i = 0; i < 4; i++) {
            void *povl = (&precord->ovlb)[i];
            void *pval = (&precord->valb)[i];
            epicsUInt32 *ponv = &(&precord->onvb)[i];
            epicsUInt32 *pnev = &(&precord->nevb)[i];
            epicsUInt32 onv = *ponv;    /* Num Elements in OVLx */
            epicsUInt32 nev = *pnev;    /* Num Elements in VALx */
            epicsUInt32 alen = dbValueSize((&precord->ftvb)[i]) * nev;

            memcpy(povl, pval, alen);
            db_post_events(precord, pval, monitor_mask);
            if (nev != onv) {
                *ponv = nev;
                db_post_events(precord, pnev, monitor_mask);
            }
        }
}

/*!
 * Puts the next value/timestamp to be archived in the "outa" link.
 *
 * \param[in,out] precord Processed record
 * \return pOutputs Whether the out-links should be processed or not.
 */
int buffer_pop(aSubRecord *precord)
{
        trace_print();
        struct buf_priv *priv;        /* Application specific data */
        void            *arcVal;      /* Target for archiving data. */
        size_t           valueSize;   /* Size of each value in array */
        epicsUInt32     *bLengthOut;  /* Actual length of array OUT. */
        epicsUInt32      bunchNo;     /* Allocated places in bunch. */
        int              pOutputs = 1;/* Return value of processing,
                                       * 0 enables out-links
                                       * 1 disables out-links */

        priv        = (struct buf_priv *) precord->dpvt;
        arcVal      =            (void *) precord->vala;
        bLengthOut  =                    &precord->neva;
        bunchNo     =                     precord->novb;
        valueSize   =                     dbValueSize(precord->fta);

        if(priv->tail->requested == 1) {
                /* Copy data to output field and set correct length. */
                memcpy(arcVal, priv->tail->data, priv->tail->nelm * valueSize);
                *bLengthOut = priv->tail->nelm;

                /* Set correct time to be archived */
                precord->time = priv->tail->time;

                /* If DOD is connected to a scalar, keep a value in the bunch array. */
                if(priv->tail->nelm == 1) {
                        memcpy((char *)priv->bunchVal + priv->cBunch * valueSize, priv->tail->data, valueSize);
                        priv->bunchTs[priv->cBunch] = priv->tail->time;
                        priv->cBunch++;
                        if(priv->cBunch == bunchNo) {
                                /* We will run out of space in the next iteration,
                                 * flip bunches */
                                flip_bunches(precord);
                        }
                }

                /* Enable processing of output links. */
                pOutputs = 0;

                /* DEBUG */
                char tmpbuf[20];
                epicsTimeToStrftime(tmpbuf,20,"%H:%M:%S.%06f",&priv->tail->time);
                debug_print("Archiving: %s\n", tmpbuf);
        } else {
                /* Make the bunch array available if the bunch is over. */
                if(priv->tail->nelm == 1 && priv->cBunch != 0) {
                        flip_bunches(precord);
                }
                char tmpbuf[20];
                epicsTimeToStrftime(tmpbuf,20,"%H:%M:%S.%06f",&priv->tail->time);
                debug_print(" Skipping: %s\n", tmpbuf);
        }

        /* Update tail to next value. */
        if(++priv->tail == priv->buf + priv->bSize) {
                priv->tail = priv->buf;
        }
        return pOutputs;
}

/*!
 * Stores input values and makes sure the head/tail-pointers are correct.
 *
 * \param[in,out] precord Processed record
 */
void buffer_push(aSubRecord *precord)
{
        trace_print();
        struct buf_priv *priv;        /* Application specific data */
        size_t           valueSize;   /* Size of each value in array */
        epicsUInt32      bLengthIn;   /* Actual length of array IN. */
        epicsUInt32      bMLengthOut; /* Maximum length of array OUT. */
        void            *data;        /* Incoming data to store. */
        unsigned int     nelm;        /* Temporary store number of elements */

        priv        = (struct buf_priv *) precord->dpvt;
        valueSize   =                     dbValueSize(precord->fta);
        bLengthIn   =                     precord->nea;
        bMLengthOut =                     precord->nova;
        data        =                     precord->a;

        /* Determine length of array to store. It isn't necessary to store more
         * data than what is possible to output. */
        if(bLengthIn < bMLengthOut) {
                nelm = bLengthIn;
        } else {
                nelm = bMLengthOut;
        }

        /* Store data. */
        memcpy(priv->head->data, data, nelm * valueSize);
        priv->head->nelm = nelm;

        /* Store timestamp from Target PV. */
        dbGetTimeStamp(&precord->inpa,  &priv->head->time);

        /* Mark as "to be archived" if appropriate */
        priv->head->requested = priv->toArchive > 0 ? 1 : 0;

        /* DEBUG */
        char tmpbuf[20];
        epicsTimeToStrftime(tmpbuf,20,"%H:%M:%S.%06f",&priv->head->time);
        debug_print("Buffering: %s %d\n", tmpbuf, priv->head->requested);

        /* Increase head ptr. If it is outside of array, reset it */
        if(++priv->head == priv->buf + priv->bSize) {
                priv->head = priv->buf;
        }

        /* Check if producer is faster than consumer */
        if(priv->tail == priv->head) {
                errlogPrintf("Data producer is faster than consumer, data is lost.\n");
                recGblSetSevr(precord,epicsAlarmSoft,epicsSevInvalid);
                priv->tail += BUFFER_SLACK-1;
                if(priv->tail >= priv->buf + priv->bSize) {
                        priv->tail -= priv->bSize;
                }
        }
}

/*!
 * This function will mark all samples as requested from head-nPre to head-1.
 * Head is where the next sample will be written.
 *
 * \param[in,out] precord Processed record
 * \param[in]     nPre    Number of elements to mark
 */
void mark_samples(aSubRecord *precord, unsigned int nPre)
{
        struct buf_priv *priv = (struct buf_priv *)precord->dpvt;
        struct buf_item *init = priv->head - nPre >= priv->buf ? priv->head - nPre : priv->head - nPre + priv->bSize;
        struct buf_item *itr = init;
        debug2_print("mark %d items, from [%ld,%ld]\n", nPre, init - priv->buf, priv->head - priv->buf - 1);
        while(itr != priv->head) {
                itr->requested = 1;
                if(++itr == priv->buf + priv->bSize) {
                        itr = priv->buf;
                }
        }
}

/*!
 * Function which is run each time the buffer record is being processed.
 * Depending on the source of the processing...
 *
 * The buffer length is MAXNPRE + BUFFER_SLACK.
 *
 * \param[in,out] precord Processed record.
 */
static long bufferProcess(aSubRecord *precord)
{
        struct buf_priv *priv;        /* Application specific data */
        size_t           valueSize;   /* Size of each value in array */
        epicsInt16      *arcFlag;     /* Flag which enables archiving (Input
                                       * from DOD-UPD). */
        epicsInt32       nPre;        /* Pre samples to store */
        epicsInt32       nPost;       /* Post samples to store */
        epicsInt16      *clkFlag;     /* Flag set by "clock" to determine
                                       * source of processing */
        epicsUInt32      bMLengthOut; /* Maximum length of array OUT. */
        epicsInt32       nMaxPre;     /* Buffer length */
        int              pOutputs = 1;/* Return value of processing,
                                       * 0 enables out-links
                                       * 1 disables out-links */
        int              init = 0;    /* used to detect PINI */
        char             action = ' ';/* Debug variable. */

        priv        = (struct buf_priv *) precord->dpvt;
        valueSize   =                     dbValueSize(precord->fta);
        arcFlag     =      (epicsInt16 *) precord->b;
        nPre        =     *(epicsInt32 *) precord->c;
        nPost       =     *(epicsInt32 *) precord->d;
        clkFlag     =      (epicsInt16 *) precord->e;
        nMaxPre     =     *(epicsInt32 *) precord->f;
        bMLengthOut =                     precord->nova;

        /* Initialize application struct if it's the first time. Write garbage
         * to the area to ensure that the memory is really there. */
        if(priv->buf == NULL) {
                buffer_reshape(precord, nMaxPre + BUFFER_SLACK);
                for(struct buf_item *itr = priv->buf; itr != priv->buf + priv->bSize; ++itr) {
                        memset(itr->data, 0xAA, MIN(precord->nova, precord->noa) * valueSize);
                }
                memset(precord->vala, 0xAA, MIN(precord->nova, precord->noa) * valueSize);
                init = 1; /* The first run is PINI */
        }

        /* Reallocate memory if buffer size changed. */
        if(priv->bSize != nMaxPre + BUFFER_SLACK) {
                debug_print("Buffer size changed from %d to %d\n",
                                priv->bSize, nMaxPre + BUFFER_SLACK);
                buffer_reshape(precord, nMaxPre + BUFFER_SLACK);
        }

        if(priv->buf == NULL) {
                errlogPrintf("Failed to initialize Data-on-Demand\n");
                return -1;
        }

        if(1 == *arcFlag) {
                /*
                 * Processed by DoD Event
                 */
                debug_print("%s\n", "Received trigger");
                mark_samples(precord, MIN(COUNT(priv->head, priv->tail, priv->bSize), nPre));
                if(priv->toArchive < nPost) {
                        priv->toArchive = nPost;
                }
                priv->initFill = 1; /* Allow reading even though it might not be filled. */
                priv->watchdog = 0;
                *arcFlag = 0;
#if DEBUG > 0
                action = 't';
#endif
        } else if(*clkFlag == 0 && !init) {
                /*
                 * Processed by data source.
                 */
                debug2_print("Buffer  (pre) s=%2u, h=%2ld, t=%2ld c=%2ld\n",
                                priv->bSize,
                                priv->head - priv->buf,
                                priv->tail - priv->buf,
                                COUNT(priv->head, priv->tail, priv->bSize));
                buffer_push(precord);
                debug2_print("Buffer (post) s=%2u, h=%2ld, t=%2ld c=%2ld\n",
                                priv->bSize,
                                priv->head - priv->buf,
                                priv->tail - priv->buf,
                                COUNT(priv->head, priv->tail, priv->bSize));

                if(priv->toArchive > 0){
                        (priv->toArchive)--;
                        priv->watchdog = 0;
                }
                /* If buffer is filled, allow reading. */
                if(!priv->initFill && COUNT(priv->head, priv->tail, priv->bSize) >= nMaxPre + 1) {
                        debug_print("%s\n", "Filled");
                        priv->initFill = 1;
                }
#if DEBUG > 0
                ++priv->writes;
                action = 'w';
#endif
        } else if (*clkFlag == 1) {
                /*
                 * Processed by "clock".
                 */
                /* Reset clock flag */
                *clkFlag = 0;
                if(priv->initFill && COUNT(priv->head, priv->tail, priv->bSize) > 0) {
                        pOutputs = buffer_pop(precord);
                } else if(priv->initFill && bMLengthOut == 1) {
                        if(priv->cBunch != 0) {
                                flip_bunches(precord);
                        }
                        priv->initFill = 0;
                }
                /* Post trigger samples has to be in the same timeframe as the
                 * trigger. If the watchdog reaches toArchive + 1, enough new raw
                 * data has been acquired to consider the next values not part
                 * of this timeframe. */
                if(priv->watchdog == priv->toArchive + 1) {
                        priv->toArchive = 0;
                }
                /* If values have been requested (toArchive > 0) increase
                 * watchdog. */
                if(priv->toArchive > 0) {
                        (priv->watchdog)++;
                }
#if DEBUG > 0
                ++priv->reads;
                action = 'r';
#endif
        }

        debug_print("a: %c, pw: %3u, pr: %3u, d: %3d, s: %3u, h: %3lu, t: %3lu, c=%3ld, w: %3d, a:%3d\n",
                        action, priv->writes, priv->reads, priv->writes - priv->reads,
                        priv->bSize, priv->head - priv->buf, priv->tail - priv->buf,
                        COUNT(priv->head, priv->tail, priv->bSize),
                        priv->watchdog, priv->toArchive);

        return pOutputs;
}

/* Register these symbols for use by IOC code: */
epicsRegisterFunction(bufferInit);
epicsRegisterFunction(bufferProcess);
